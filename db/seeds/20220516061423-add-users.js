'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert('Users',
      [{
          name: "Kim Namjoon",
          email: "rkive@gmail.com",
          password: "$2a$10$vPao1I29yjnw5fuRRdDeD.1JOUaJf/BFmgQupGqiFySaJBjZPz1Yu",
          role: "superadmin",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "Safira Tyas Wandita",
          email: "safiratyas@gmail.com",
          password: "$2a$10$0Fav3lhIOr08mlPY.Im5Y.5A6LugSoU5m3h4tamoxxJCatheWZ2O6",
          role: "admin",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "Taylor Swift",
          email: "swifties13@gmail.com",
          password: "$2a$10$TGNQgFbNU5evU.6mLjnuAe0DP1p3K9UQpjryQDxzPVSZ9IPImRJna",
          role: "member",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ]
    )
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};